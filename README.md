# 5 min PostgreSQL Analysis

## Scope

![PostgreSQL LOGO](postgressql.png) This is an example project that let you analyze and review the code of **PostgreSQL 16.3** in 5 min or less **WITHOUT USING A DOCKER IMAGE**.

This assume you have an ubuntu 22.04+ machine ready to go!

If you prefer to use Docker, refer to this [project](https://gitlab.com/EmendaUSA/demos/5-min-postgressql-analysis)

## Requirements

- [ ] Have a license key for SciTools Understand with DevOps Enabled (Later, we will say the key is (LICENSE_KEY))
- [ ] We assume the host is Ubuntu 22.04+, but feel free to adjust if you want to venture out.

**Have Questions?** : Contact US.Sales@Emenda.com

## You are the doing it! Great! Ready, Set, Go!

First, we are going to set up the machine for Understand.

```
# UPGRADE AND UPDATE
sudo apt-get update 
sudo apt-get upgrade -y

# INSTALL SCITOOLS DEPENDENCIES
sudo apt-get install -y \
libxcb-xinerama0 libxkbcommon-x11-0 libxcb-shape0 libxcb-cursor0 \
libxcb-icccm4 libxcb-image0 libxcb1 libxcb-keysyms1 \
libxcb-render-util0 libglib2.0-dev language-pack-en-base dnsutils

# INSTALL UTILS
sudo apt-get install -y git zip curl
sudo apt-get install -y build-essential bear dos2unix

mkdir ~/scitools_dir
cd ~/scitools_dir

# INSTALL scitools-essential (helper package)
git clone https://gitlab.com/EmendaUSA/scitools-essential
chmod a+x ./scitools-essential/bash_scripts/*.bash
dos2unix ./scitools-essential/bash_scripts/*.bash
rm -Rf ./scitools-essential/.git

# INSTALL scitools itself
curl --output Understand.64bit.tgz --url https://s3.amazonaws.com/builds.scitools.com/all_builds/b1185/Understand/Understand-6.5.1185-Linux-64bit.tgz
tar -xvzf output Understand.64bit.tgz
rm output Understand.64bit.tgz
# Bug Fix. Erase the convertToPDF tool as it would fail in CLI
rm ~/scitools_dir/scitools/bin/linux64/convertToPDF
echo "" > ~/scitools_dir/scitools/bin/linux64/convertToPDF
chmod a+x ~/scitools_dir/scitools/bin/linux64/convertToPDF
# Bug Fix End

# SETUP PATH.
export PATH=$PATH:~/scitools_dir/scitools/bin/linux64
export PATH=$PATH:~/scitools_dir/scitools-essential/bash_scripts
```

Now, we are going to install the postgreSQL repo itself!

```
cd ~
sudo apt-get install build-essential libreadline-dev zlib1g-dev flex bison libxml2-dev libxslt-dev libssl-dev libxml2-utils xsltproc ccache pkg-config
git clone https://github.com/postgres/postgres.git
cd postgres
git checkout REL_16_3
./configure
```

We build the project now using bear (to create compile_commands.json)

```
bear -- make
```

Make sure to replace (LICENSE_KEY) by your license key!
```
und -setlicensecode (LICENSE_KEY)
```

Finally we create the scitools database!

```
cd ~/postgres
create_database_advanced.bash -db scitools.und -cpp -gcc gcc
remove_symlinks.bash -dir . -pattern "*.[ch]*"
und add -cmake ./compile_commands.json -analyzelater -onetime -db scitools.und 
und analyze -db scitools.und
```

You are done! Run your project with the following (if you have a GUI! If not, see next section...) 
```
understand scitools.und
```

This will open Understand with PostgresSQL analyzed!

Enjoy!

## This is only a build machine, I can't run a GUI.

No problem, add the following lines just after the `und analyze -db scitools.und`

```
zip_database_advanced.bash -db scitools.und/ -zip scitools.zip
```

Bring the scitools.zip on a machine with GUI, and then run:

```
und -setlicensecode (LICENSE_KEY)
cd (DIRECTORY)
tar.exe -xf .\scitools.zip
und settings -C++AddFoundSystemFiles off -db .\scitools.und
und add -root DEPENDENCIES_USR=".\usr" -db .\scitools.und
und analyze -db .\scitools.und
understand .\scitools.und
```

## I get 116 errors, is that normal?

Well, yes and no. 

Understand C++ Engine is build on top of CLANG, and the code is GCC Code. In this specific example, all the errors are in /usr/lib/gcc/x86_64-linux-gnu/ folder, **Not in the PostgresSQL code itself**. All those errors relates to built-in x86_64 instructions that conflict between CLANG and GCC. 

We can fix it by hiding the problematic definition "deep" in the gcc header files. To do so, simply use the [fix.ini](./fix.ini) file using:
```
und settings -C++MacrosAdd @".\fix.ini" -db .\scitools.und
```

There is only one in .\src\common\config_info.c, "expected expression". That one is due to an error parsing the compile_commands.json for macro VAL_CFLAGS for file `.\src\common\config_info.c`...

To fix it, simply run:
```
und settings -override_c++_macrosAdd ".\src\common\config_info.c" VAL_CFLA
GS="\"\"" -db .\scitools.und
```

**Conclusion: You good to go! Should be 0 error now.**
